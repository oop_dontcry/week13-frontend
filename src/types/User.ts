export default interface Product {
  id?: number;
  name: string;
  password: string;
  gender: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
