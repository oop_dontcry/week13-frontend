import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type User from "@/types/User";
import userService from "@/services/user";

export const useUserStore = defineStore("user", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const users = ref<User[]>([]);
  const editedUser = ref<User>({ name: "", password: "", gender: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = { name: "", password: "", gender: "" };
    }
  });
  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("Can't get this User.");
    }
    loadingStore.isLoading = false;
  }
  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }

      dialog.value = false;
      await getUsers();
    } catch (e) {
      messageStore.showError("Can't save this User.");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await userService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("Can't delete this User.");
    }
    loadingStore.isLoading = false;
  }
  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }
  return {
    users,
    getUsers,
    dialog,
    editedUser,
    saveUser,
    editUser,
    deleteUser,
  };
});
